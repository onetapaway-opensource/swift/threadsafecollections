import XCTest

import ThreadSafeCollectionsTests

var tests = [XCTestCaseEntry]()
tests += ThreadSafeCollectionsTests.allTests()
XCTMain(tests)
